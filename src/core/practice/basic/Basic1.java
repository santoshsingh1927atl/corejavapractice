package core.practice.basic;

import java.util.*;
@FunctionalInterface
interface Left{
	void m2();
	default void m1() {
		System.out.println("Left");
	};
}
interface Right{
	default void m1() {
		System.out.println("Right");
	};
}

public class Basic1 implements Left, Right{

	public static void main(String[] args) {
		
		Left a = () -> System.out.println("Lambda");
		a.m2();
		a.m1();
		
		Right b = new Basic1();
		b.m1();
		
		Basic1 c = new Basic1();
		c.m1();
		
		Left d = new Basic1();
		d.m2();
	}

	@Override
	public void m1() {
		System.out.println("Basic M1");
		Left.super.m1();
	}

	@Override
	public void m2() {
		System.out.println("Basic M2");
	}

}
